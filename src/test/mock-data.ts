import { Post } from '@/post/post'

export const postsToTest: Post[] = Array.from(
  { length: 6 },
  (_, postIndex): Post => {
    const postId = `${postIndex + 1}`

    return {
      id: postId,
      url: 'https://example.com',
      title: `Post ${postId}`,
      description: `This is post ${postId}.`,
      image: `https://via.placeholder.com/960x540.png?text=Post+${postId}`,
      sharedBy: `User ${postId}`,
    }
  },
)
