import axios from 'axios'
import hashObj from 'hash-obj'
import metascraperBuilder, { Metadata } from 'metascraper'
import metascraperDescription from 'metascraper-description'
import metascraperImage from 'metascraper-image'
import metascraperTitle from 'metascraper-title'
import normalizeUrl from 'normalize-url'

import { Post } from '@/post/post'
import { PostList } from '@/post/post-list'
import { postsToTest } from '@/test/mock-data'

const metascraper = metascraperBuilder([metascraperTitle(), metascraperDescription(), metascraperImage()])
const metadataByUrl: Record<string, Metadata> = {}

const getMetadata = async (url: string): Promise<Metadata> => {
  const cachedMetadata = metadataByUrl[url]

  if (cachedMetadata) {
    return cachedMetadata
  }

  const html = (await axios.get(url)).data
  const metadata: Metadata = await metascraper({ html, url })

  metadataByUrl[url] = metadata

  return metadata
}

const decoratePost = async (post: Post): Promise<Post> => {
  if (process.env.USE_MOCK === 'true') {
    return post
  }

  try {
    const metadata = await getMetadata(post.url)

    console.log(`received metadata of ${post.url}`)

    return {
      ...post,
      title: metadata.title || post.title,
      description: metadata.description || '',
      image: metadata.image || '',
    }
  } catch (err) {
    return post
  }
}

const fetchPosts = async (): Promise<Post[]> => {
  if (process.env.USE_MOCK === 'true') {
    return postsToTest
  }

  const sheetData = (
    await axios.get(
      `https://spreadsheets.google.com/feeds/list/${process.env.POST_GOOGLE_SHEET_ID}/od6/public/values?alt=json`,
    )
  ).data

  console.log(`received ${sheetData.feed.entry.length} entries`)

  const posts: Post[] = []

  for (const entry of sheetData.feed.entry) {
    try {
      const urlText = entry['gsx$link']['$t']
      const sharedByText = entry['gsx$sharedby']['$t']
      const url = normalizeUrl(urlText)
      const id = hashObj({ url }, { algorithm: 'md5' })

      posts.push({
        id,
        url,
        title: url,
        sharedBy: sharedByText,
      })
    } catch (err) {}
  }

  return posts
}

const postsPerPage = 4

export const getPostIdsToGenerate = async (): Promise<string[]> => {
  const allPosts = await fetchPosts()
  return allPosts.slice(0, postsPerPage).map(({ id }) => id)
}

export const getPostById = async (postId: string): Promise<Post | undefined> => {
  const allPosts = await fetchPosts()
  const post = allPosts.find(({ id }) => id === postId)

  if (!post) {
    return
  }

  return decoratePost(post)
}

const getTotalPages = (totalPosts: number): number => {
  return Math.ceil(totalPosts / postsPerPage)
}

export const getAllPageIds = async (): Promise<number[]> => {
  const allPosts = await fetchPosts()
  const totalPages = getTotalPages(allPosts.length)
  return Array.from({ length: totalPages }, (_, pageIndex) => pageIndex + 1)
}

export const getPostListByPageId = async (pageId: number): Promise<PostList> => {
  const allPosts = await fetchPosts()
  const totalPages = getTotalPages(allPosts.length)
  const postsOfPage = allPosts.slice((pageId - 1) * postsPerPage, pageId * postsPerPage)

  const decoratedPosts: Post[] = []

  for (const post of postsOfPage) {
    decoratedPosts.push(await decoratePost(post))
  }

  return {
    posts: decoratedPosts,
    pageId,
    ...(pageId > 1 ? { prevPageId: pageId - 1 } : {}),
    ...(pageId < totalPages ? { nextPageId: pageId + 1 } : {}),
  }
}
