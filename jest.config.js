module.exports = {
  testTimeout: 20000,
  moduleNameMapper: {
    '@/(.*)': '<rootDir>/src/$1',
  },
}
