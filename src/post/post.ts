export interface Post {
  readonly id: string
  readonly url: string
  readonly title: string
  readonly description?: string
  readonly image?: string
  readonly sharedBy?: string
}
