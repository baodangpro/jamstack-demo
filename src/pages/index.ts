import { GetStaticProps } from 'next'

import { HomePage, HomePageProps } from '@/home/home-page'
import { getPostListByPageId } from '@/post/post-data'

export default HomePage

export const getStaticProps: GetStaticProps<HomePageProps> = async () => {
  console.log('Generating home')

  return {
    props: {
      postList: await getPostListByPageId(1),
    },
    revalidate: 10,
  }
}
