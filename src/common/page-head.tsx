import NextHead from 'next/head'
import { FunctionComponent } from 'react'

export interface PageHeadProps {
  readonly title: string
}

export const PageHead: FunctionComponent<PageHeadProps> = ({ title }) => {
  return (
    <NextHead>
      <meta name='viewport' content='width=device-width, initial-scale=1' />
      <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bulma@0.9.2/css/bulma.min.css' />
      <title>{title} - Jamstack Demo</title>
    </NextHead>
  )
}
