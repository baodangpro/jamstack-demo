import { FunctionComponent } from 'react'

import { ButtonLink } from '@/common/button-link'
import { TextLink } from '@/common/text-link'
import { PostList } from '@/post/post-list'

export interface PostListDisplayProps {
  readonly postList: PostList
}

export const PostListDisplay: FunctionComponent<PostListDisplayProps> = ({ postList }) => {
  return (
    <>
      <div className='content'>
        <ul>
          {postList.posts.map((post) => (
            <li key={post.id}>
              <TextLink href={`/posts/${post.id}`}>{post.title}</TextLink>
              {post.sharedBy && <strong> [{post.sharedBy}]</strong>}
            </li>
          ))}
        </ul>
      </div>
      <div className='buttons'>
        {postList.prevPageId && (
          <ButtonLink href={postList.prevPageId === 1 ? '/' : `/posts/p/${postList.prevPageId}`} className='is-link'>
            Previous
          </ButtonLink>
        )}
        {postList.nextPageId && (
          <ButtonLink href={`/posts/p/${postList.nextPageId}`} className='is-link'>
            Next
          </ButtonLink>
        )}
      </div>
    </>
  )
}
