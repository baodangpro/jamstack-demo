# Jamstack Demo

- [https://www.toptal.com/developers/gitignore](https://www.toptal.com/developers/gitignore) Create useful .gitignore files for your project

- [https://tsdx.io](https://tsdx.io) Zero-config CLI for TypeScript development

- [https://nextjs.org](https://nextjs.org) Next.js gives you the best React developer experience with all the features you need for production: hybrid static & server rendering, TypeScript support, smart bundling, route pre-fetching, and more. No config needed.

- [https://vercel.com](https://vercel.com) Vercel combines the best developer experience with an obsessive focus on end-user performance. Our platform enables frontend teams to do their best work.

- [https://unsplash.com](https://unsplash.com) Over 2 million free high-resolution images brought to you by the world’s most generous community of photographers.

- [https://github.com/microlinkhq/metascraper](https://github.com/microlinkhq/metascraper) Scrape data from websites using Open Graph, HTML metadata & fallbacks.

- [https://jamstack.org](https://jamstack.org) Jamstack is an architecture designed to make the web faster, more secure, and easier to scale. It builds on many of the tools and workflows which developers love, and which bring maximum productivity.

- [https://bulma.io](https://bulma.io) Bulma is a free, open source framework that provides ready-to-use frontend components that you can easily combine to build responsive web interfaces.

- [https://github.com/americanexpress/jest-image-snapshot](https://github.com/americanexpress/jest-image-snapshot) Jest matcher for image comparisons. Most commonly used for visual regression testing.
