import { NextPage } from 'next'

import { PageHead } from '@/common/page-head'
import { PostList } from '@/post/post-list'
import { PostListDisplay } from '@/post/post-list-display'

export interface HomePageProps {
  readonly postList: PostList
}

export const HomePage: NextPage<HomePageProps> = ({ postList }) => {
  return (
    <>
      <PageHead title='Home' />
      <main>
        <div className='container is-max-desktop my-6 px-3'>
          <h1 className='title'>Posts</h1>
          <PostListDisplay postList={postList} />
        </div>
      </main>
    </>
  )
}
