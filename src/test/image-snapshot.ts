import { toMatchImageSnapshot } from 'jest-image-snapshot'
import { Browser, launch } from 'puppeteer-core'

require('dotenv-flow').config()

expect.extend({ toMatchImageSnapshot })

export const launchBrowser = (): Promise<Browser> => {
  return launch({
    executablePath: process.env.CHROME_PATH || '/usr/bin/google-chrome',
  })
}

export const testPage = async (browser: Browser, pagePath: string): Promise<void> => {
  const page = await browser.newPage()
  await page.goto(`http://localhost:3000${pagePath}`)

  const mainElement = await page.$('main')
  if (!mainElement) {
    throw new Error('No main element')
  }

  const image = await mainElement.screenshot()

  const pageId = pagePath.replace(/[/_]/g, '-').replace(/^-+/, '') || 'home'
  expect(image).toMatchImageSnapshot({
    customSnapshotIdentifier: `page-${pageId}`,
    failureThreshold: 0.01,
    failureThresholdType: 'percent',
  })
}
