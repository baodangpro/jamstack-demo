import { FunctionComponent } from 'react'

import { Post } from '@/post/post'

export interface PostDisplayProps {
  readonly post: Post
}

export const PostDisplay: FunctionComponent<PostDisplayProps> = ({ post }) => {
  return (
    <div style={{ maxWidth: 500, margin: '0 auto' }}>
      <h1 className='title'>
        <a href={post.url}>{post.title}</a>
      </h1>
      {post.sharedBy && (
        <p className='subtitle'>
          <strong>{post.sharedBy}</strong>
        </p>
      )}
      {post.image && (
        <div className='block'>
          <figure className='image is-16by9'>
            <img src={post.image} alt={post.title} style={{ objectFit: 'cover' }} />
          </figure>
        </div>
      )}
      {post.description && (
        <div className='block'>
          <p className='is-size-4'>{post.description}</p>
        </div>
      )}
    </div>
  )
}
