import { Post } from '@/post/post'

export interface PostList {
  readonly posts: Post[]
  readonly pageId: number
  readonly prevPageId?: number
  readonly nextPageId?: number
}
