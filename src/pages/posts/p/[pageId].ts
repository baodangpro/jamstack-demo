import { GetStaticPaths, GetStaticProps } from 'next'
import { ParsedUrlQuery } from 'querystring'

import { getAllPageIds, getPostListByPageId } from '@/post/post-data'
import { PostListPage, PostListPageProps } from '@/post/post-list-page'

export default PostListPage

export interface PostListPageParams extends ParsedUrlQuery {
  readonly pageId: string
}

export const getStaticPaths: GetStaticPaths<PostListPageParams> = async () => {
  const allPageIds = await getAllPageIds()

  return {
    paths: allPageIds.map((pageId) => ({ params: { pageId: `${pageId}` } })),
    fallback: false,
  }
}

export const getStaticProps: GetStaticProps<PostListPageProps, PostListPageParams> = async ({ params }) => {
  const pageId = params && params.pageId
  const postList = pageId ? await getPostListByPageId(+pageId) : undefined

  if (!postList || !postList.posts.length) {
    return {
      notFound: true,
    }
  }

  console.log(`Generating post list ${pageId}`)

  return {
    props: {
      postList,
    },
    revalidate: 10,
  }
}
