import { NextPage } from 'next'

import { PageHead } from '@/common/page-head'
import { TextLink } from '@/common/text-link'
import { PostList } from '@/post/post-list'
import { PostListDisplay } from '@/post/post-list-display'

export interface PostListPageProps {
  readonly postList: PostList
}

export const PostListPage: NextPage<PostListPageProps> = ({ postList }) => {
  return (
    <>
      <PageHead title='Posts' />
      <main>
        <div className='container is-max-desktop my-6 px-3'>
          <TextLink href='/'>Home</TextLink>
          <h1 className='title'>Posts</h1>
          <PostListDisplay postList={postList} />
        </div>
      </main>
    </>
  )
}
