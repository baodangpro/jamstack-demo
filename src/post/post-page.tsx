import { NextPage } from 'next'
import { useRouter } from 'next/router'

import { PageHead } from '@/common/page-head'
import { TextLink } from '@/common/text-link'
import { Post } from '@/post/post'
import { PostDisplay } from '@/post/post-display'

export interface PostPageProps {
  readonly post: Post
}

export const PostPage: NextPage<PostPageProps> = ({ post }) => {
  const router = useRouter()

  if (router.isFallback) {
    return <div>Loading...</div>
  }

  return (
    <>
      <PageHead title={post.title} />
      <main>
        <div className='container is-max-desktop my-6 px-3'>
          <TextLink href='/'>Home</TextLink>
          <PostDisplay post={post} />
        </div>
      </main>
    </>
  )
}
