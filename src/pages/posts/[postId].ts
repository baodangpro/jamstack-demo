import { GetStaticPaths, GetStaticProps } from 'next'
import { ParsedUrlQuery } from 'querystring'

import { getPostById, getPostIdsToGenerate } from '@/post/post-data'
import { PostPage, PostPageProps } from '@/post/post-page'

export default PostPage

export interface PostPageParams extends ParsedUrlQuery {
  readonly postId: string
}

export const getStaticPaths: GetStaticPaths<PostPageParams> = async () => {
  const allPostIds = await getPostIdsToGenerate()

  return {
    paths: allPostIds.map((postId) => ({ params: { postId } })),
    fallback: true,
  }
}

export const getStaticProps: GetStaticProps<PostPageProps, PostPageParams> = async ({ params }) => {
  const postId = params && params.postId
  const post = postId ? await getPostById(postId) : undefined

  if (!post) {
    return {
      notFound: true,
    }
  }

  console.log(`Generating post ${post.url}`)

  return {
    props: {
      post,
    },
    revalidate: 10,
  }
}
