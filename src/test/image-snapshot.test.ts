import { Browser } from 'puppeteer-core'

import { launchBrowser, testPage } from '@/test/image-snapshot'

describe('Image snapshot', () => {
  let browser: Browser

  beforeAll(async () => {
    browser = await launchBrowser()
  })

  it('should track home page', async () => {
    await testPage(browser, '/')
  })

  it('should track post list page', async () => {
    await testPage(browser, '/posts/p/2')
  })

  it('should track post page', async () => {
    await testPage(browser, '/posts/1')
  })

  afterAll(async () => {
    await browser.close()
  })
})
